from starlette.testclient import TestClient
from app.views import app

client = TestClient(app)

ENDPOINT = "wishlist/10"


def test_main_status_code():
    response = client.delete(ENDPOINT)
    assert response.status_code == 200


def test_listar_response_json():
    response = client.delete(ENDPOINT)
    assert response.json() == "produto removido!"
