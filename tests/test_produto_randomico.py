from starlette.testclient import TestClient
from app.views import app

client = TestClient(app)

ENDPOINT = "wishlist/randomico"


def test_main_status_code():
    response = client.get(ENDPOINT)
    assert response.status_code == 200


def test_listar_randomico_response_json():
    response = client.get(ENDPOINT)
    assert len(response.json()) == 6
