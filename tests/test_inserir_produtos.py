import json
from starlette.testclient import TestClient
from app.views import app

client = TestClient(app)

ENDPOINT = "wishlist/"

headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json',
}

data = '{"titulo":"produto teste","descricao":"ssd, i5","link":"amazon.com","foto":"static.com"}'


def test_main_status_code():
    response = client.post(ENDPOINT, headers=headers, data=data)
    assert response.status_code == 201


def test_listar_response_json():
    response = client.post(ENDPOINT, data=data, headers=headers)
    assert response.json() == "produto inserido!"