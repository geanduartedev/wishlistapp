from starlette.testclient import TestClient
from app.views import app


client = TestClient(app)

ENDPOINT = "wishlist/7?status_produto=Ganhei"


def test_main_status_code():
    response = client.put(ENDPOINT)
    assert response.status_code == 200


def test_listar_response_json():
    response = client.put(ENDPOINT)
    assert response.json() == "status alterado!"
