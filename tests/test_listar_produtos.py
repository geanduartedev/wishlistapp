from starlette.testclient import TestClient
from app.views import app
from app.api import produtos

client = TestClient(app)

ENDPOINT = "/wishlist"


def test_main_status_code():
    response = client.get(ENDPOINT)
    assert response.status_code == 200


def test_listar_response_json():
    response = client.get(ENDPOINT)
    assert len(response.json()) == produtos.select().count()
