from peewee import TextField, ForeignKeyField
from app.db import BaseModel


class Pessoa(BaseModel):
    nome = TextField(unique=True)


class Wishlist(BaseModel):
    nome = TextField(unique=True)
    dona = ForeignKeyField(Pessoa, backref='wishlists')


class Produto(BaseModel):
    wishlist = ForeignKeyField(Wishlist, backref='produtos')
    titulo = TextField()
    descricao = TextField()
    link = TextField()
    foto = TextField()
    status = TextField()




