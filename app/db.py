from peewee import Model, PostgresqlDatabase
from os import getenv
from dotenv import load_dotenv

load_dotenv()

data_base = PostgresqlDatabase(
    database=getenv('DB_NAME'),
    user=getenv('DB_USER'),
    password=getenv('DB_PASSWORD'),
    host=getenv('DB_HOST'),
    port=getenv('DB_PORT'),
    autorollback=True
)


class BaseModel(Model):
    class Meta:
        database = data_base

