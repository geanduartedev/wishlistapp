from starlette.testclient import TestClient

from app.views import app

client = TestClient(app)

"""
testar criar produto
testar deletar produto
testar listar produtos
testar sortear produtos
alterar o status dos produtos
"""


def test_main_status_code():
    response = client.get("/")
    assert response.status_code == 200

"""
def test_main_reponse_json():
    response = client.get("/")
    assert response.json() == {"ola": "mundo"}

def test_listar_reponse_json():
    response = client.get("/a-fazer")
    assert len(response.json()) == 3

"""