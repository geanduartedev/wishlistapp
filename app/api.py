from random import choice
from fastapi.routing import APIRouter
from typing import List, Optional
from app.schemas import ModeloDoItem, ModeloDoItemResposta, OpcoesDeStatus
from app.models import Produto

router = APIRouter()
produtos = Produto()


@router.get("/", response_model=List[ModeloDoItemResposta])
def listar_produtos(status_produto: Optional[OpcoesDeStatus] = None):
    """
    Endpoint que retorna a lista de produtos com a opção de filtrar por status
    """
    if status_produto is not None:
        return list(produtos.select().where(Produto.status == status_produto).dicts())
    return list(produtos.select().dicts())


@router.post("/", status_code=201)
def inserir_produto(produto_a_inserir: ModeloDoItem):
    """
    Endpoint que insere um produto na lista de desejos
    """

    produto = produto_a_inserir.dict()
    produto["wishlist"] = 1
    produtos.insert_many(produto).execute()
    return 'produto inserido!'


@router.delete("/{id_do_produto}")
def remover_produto(id_do_produto: int):
    """
    Endpoint que remove um produto a partir do id dele
    """
    produtos.delete_by_id(id_do_produto)
    return 'produto removido!'


@router.put("/{id_do_produto}")
def atualizar_status_produto(id_do_produto: int, status_produto: Optional[OpcoesDeStatus]):
    """
    Endpoint que atualiza o status de um produto a partir do id dele e da opção de status
    """
    produto = produtos.select().where(Produto.id == id_do_produto).get()
    produto.status = status_produto
    produto.save()
    return "status alterado!"


@router.get("/randomico", response_model=ModeloDoItemResposta)
def obter_produto_randomico():
    """
    Endpoint que pega um produto de forma randomica
    """
    return choice(list(produtos.select().dicts()))
