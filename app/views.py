from fastapi import FastAPI, Request
from app.api import router as wish_list
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates


templates = Jinja2Templates(directory="templates")
description = """
A API WishListApp ajuda você a fazer coisas incríveis. 🚀

## WishList (Lista de desejos)

* Você pode **listar todos produtos e filtrar por status (opcional)** da lista de desejos (wishlist).
* Você pode **inserir um novo produto** na lista de desejos(wishlist).
* Você pode **alterar o status do produto** da lista de desejos(wishlist).
* Você pode **remover o produto** da lista de desejos(wishlist).
* Você pode **sortear um pruduto** da lista de desejos (wishlist).

## Clientes

Você poderá:

* **Criar usuários** (_não implementado_).
* **Ler usuários** (_não implementado_).
* **Criar novas WishList** (_não implementado_).
* **Validação dos fields** (_não implementado_).



"""

app = FastAPI(
    title="WishListApp",
    description=description,
    version="0.0.3",
    contact={
        "name": "Canal desenvolvedor",
        "url": "http://127.0.0.1:8000/#",
        "email": "gean_sd@live.com",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)

app.mount("/static", StaticFiles(directory="static"), name="static")
app.include_router(wish_list, prefix="/api/wishlist", tags=["WishList"])


@app.get("/", response_class=HTMLResponse, include_in_schema=False)
def index(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

