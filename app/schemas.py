from pydantic import BaseModel
from typing import Optional
from enum import Enum


class OpcoesDeStatus(str, Enum):
    nao_comprei = "Não Comprei"
    ganhei = "Ganhei"
    comprei = "Comprei"


class ModeloDoItem(BaseModel):
    titulo: str
    descricao: str
    link: str
    foto: str
    status: Optional[OpcoesDeStatus] = 'Não Comprei'


class ModeloDoItemResposta(BaseModel):
    id: int
    titulo: str
    descricao: str
    link: str
    foto: str
    status: Optional[OpcoesDeStatus] = 'Não Comprei'
